import asyncio
import re, datetime
import aiohttp

# я зарегистрировался, поэтому использую личный api_key

API_KEY = '029941ddd1121916d4e5261bc81b50a8'

# Создаем класс
class City_Weather():
    def __init__(self, city_name, city_temp, city_feels_like, city_humidity, city_pressure):
        self.city_name = city_name
        self.city_temp = city_temp
        self.city_humidity = city_humidity
        self.city_pressure = city_pressure
        self.city_feels_like = city_feels_like

# Функция проверки значений на совпадения

def weather_pars(r_text):
    city_name = re.search(r'name":"[a-zA-Z -]+',r_text).group(0).split(":")[1].replace('"',"")
    city_temp = re.search(r'temp":-?(\d+(\.\d+)?)',r_text).group(0).split(":")[1] # Так как погода приобрела отрицательные значения добавлен "минус"
    city_feels_like = re.search(r'feels_like":-?(\d+(\.\d+)?)',r_text).group(0).split(":")[1] # Добавлено значение "Ощущается как"
    city_humidity = re.search(r'humidity":\d+',r_text).group(0).split(":")[1]
    city_pressure = re.search(r'pressure":\d+',r_text).group(0).split(":")[1]
    current_city_weather = City_Weather(city_name, city_temp, city_feels_like, city_humidity, city_pressure)
    return current_city_weather

# Подключаемся к openweathermap.org 

async def get_page_data(city_name):
    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(ssl=False)) as session:
        url = f"https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid={API_KEY}&units=metric"
        async with session.get(url=url) as response:
                if response.status == 200:
                    print("*" * 30)
                    current_city_weather = weather_pars(await response.text())
                    print(f'Город:\t{current_city_weather.city_name}\n' \
                          f'Температура: {current_city_weather.city_temp} C°\n' \
                          f'Ощущается как: {current_city_weather.city_feels_like} C°\n'\
                          f'Влажность: {current_city_weather.city_humidity} %\n' \
                          f'Давление: {current_city_weather.city_pressure} мм р.т.')
                    
start_time = datetime.datetime.now()

# В этой функции будем импортировать наименования городов из файла, сохранять их в список,
# а затем создавать задачи для паралльного запуска
async def main():

# Извлекаем наименования городов из файла и сохраняем в список

    with open("cities.txt", "r") as file:
        cities = file.read()

    city_names = []

    for city in cities.split("\n"):
        city_name = city.split()[1]
        if city_name:
            city_names.append(city_name)

# Редактируем импортированный список: из наименований городов, 
# которые состоят из двух слов спарсились только первые слова - находим, 
# удаляем и вставляем полное наименование города в то же место списка, 
# а также удаляем первое значение списка - "городов:"

    del city_names[0]

    if 'Saint' in city_names:
        city_names.remove('Saint')
        city_names.insert(1, 'Saint Petersburg')

    if 'Nizhny' in city_names:
        city_names.remove('Nizhny')
        city_names.insert(5, 'Nizhny Novgorod')

# Создаем задачи для каждого населенного пункта и сохраняем их в список 
# с целью запуска паралльного запроса

    tasks = []
    for city in city_names:
        task = asyncio.create_task(get_page_data(city))
        tasks.append(task)
    await asyncio.gather(*tasks)

# Выводим результат

asyncio.run(main())

print('\nTotal time jobs: ', datetime.datetime.now() - start_time)


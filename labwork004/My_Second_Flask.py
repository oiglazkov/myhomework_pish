from flask import Flask, render_template, send_from_directory
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy import text
from dbconfig import MZ_DWH # файл с параметрами подключения к базе данных

host, port, user, password, database = MZ_DWH() # вызываю аргументы функции MZ_DWH() для подключения к базе данных

class Base(DeclarativeBase):
  pass

db = SQLAlchemy(model_class=Base)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = f"postgresql+psycopg2://{user}:{password}@{host}:{port}/{database}"
db.init_app(app)


@app.route('/styles.css')
def send_styles():
    return send_from_directory('templates', 'styles.css') # путь к файлу стилей

@app.route('/')
def index():
    with db.engine.connect() as connection:
        result = connection.execute(text("""with cte_table2 as (
                                            select 
                                            se.region_name as "Субъект РФ", 
                                            se.sp_oid as "OID Мед. орг.",
                                            se.sp_name as "Имя мед. орг."
                                            from db2.session_success se
                                            group by
                                            se.region_name,
                                            se.sp_oid,
                                            se.sp_name
                                            )
                                         select distinct * from cte_table2
                                            where "OID Мед. орг." is not null
                                            and "Субъект РФ" is not null
                                            and "Имя мед. орг." is not null
                                         group by
                                            "Субъект РФ" ,
                                            "OID Мед. орг." ,
                                            "Имя мед. орг."
                                         limit 20;""")
                                            )
        # for row in result: # для проверки вывода строк в терминале
        #   print(row)
        # return 'Запрос выполнен успешно'
        return render_template('index.html', result=result) # для отображения результата запроса передаем переменную result в шаблон
        # шаблон по умолчанию должен храниться в папке templates/

if __name__ == '__main__':
    app.run(debug=True) 
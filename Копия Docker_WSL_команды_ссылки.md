

## Установка WSL в Windows

Установка WSL
https://learn.microsoft.com/ru-ru/windows/wsl/install
https://lumpics.ru/installing-wsl-in-windows-10/

Команды WSL
https://learn.microsoft.com/ru-ru/windows/wsl/basic-commands#install

Устранение неполадок с установкой WSL
https://learn.microsoft.com/ru-ru/windows/wsl/troubleshooting#installation-issues

Перенос WSL Ubuntu на другой диск 2 способа
https://dev.to/mefaba/installing-wsl-on-another-drive-in-windows-5c4a
https://github.com/pxlrbt/move-wsl


## Установка Docker

Установка WSL Ubuntu22.04 + установка Docker
https://tjisblogging.blogspot.com/2022/05/install-docker-on-windows-11-with-wsl.html

Docker установка в Ubuntu
https://docs.docker.com/engine/install/ubuntu/ 
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04 

Докер Desktop установка в Windows (через WSL или HYPER-V)
https://www.docker.com/get-started/ 
https://docs.docker.com/desktop/install/windows-install/ 
После установки через гипервизор перезагрузить, если висит надпись Starting - попробовать способы отсюда 
https://stackoverflow.com/questions/43041331/docker-forever-in-docker-is-starting-at-windows-task 

Установка Vbox Ubuntu + Python + Docker
https://www.youtube.com/watch?v=2FkDJPPm8kk 

Основы докер + шпаргалка по командам
https://www.youtube.com/watch?v=eAXjeh5MRVU 
https://cloud.mail.ru/public/PFT8/GRwLzPvJK 

Docker Desktop через WSL туториал + VS code
https://learn.microsoft.com/ru-ru/windows/wsl/tutorials/wsl-containers 
https://habr.com/ru/articles/474346/ 


## Материалы по Docker

Плейлисты по Docker	
https://www.youtube.com/playlist?list=PLqVeG_R3qMSwjnkMUns_Yc4zF_PtUZmB-
https://www.youtube.com/playlist?list=PLQJ7ptkRY-xbR0ka2TUxJkXna40XWu92m

Часовое видео + шпаргалка по Docker	
https://www.youtube.com/watch?v=eAXjeh5MRVU 
https://cloud.mail.ru/public/PFT8/GRwLzPvJK 

Серия статей про Docker
https://habr.com/ru/companies/ruvds/articles/438796/

Сайт где можно поработать с Docker в Online (нужна авторизация через dockerhub, github или gmail)
https://labs.play-with-docker.com/

Туториал по созданию Flask приложения
https://docs.docker.com/language/python/build-images/ 

Курс Docker для начинающих Stepik
https://stepik.org/course/123300/info 

Шпаргалка с командами Docker
https://habr.com/ru/companies/flant/articles/336654/ 


-------------------------------
Как уменьшить размер диска системы WSL если после докера он стал занимать очень много места
1. Очистить кэш сборки образов
```
docker builder prune -af
```
После завершения операции внизу будет показано освобожденное пространство

2. Оптимизация диска системы WSL через PowerShell, запущенный от админа
```
wsl --shutdown
cd WSL_DISK_PATH
optimize-vhd -Path .\ext4.vhdx -Mode full
```
Вместо WSL_DISK_PATH подставить свой путь до диска системы, например
C:\Users\UserName\AppData\Local\Packages\DistroName\LocalState

Пример названия директории DistroName
CanonicalGroupLimited.UbuntuonWindows_79rhkp1fndgsc

Удобно заранее переместить WSL на другой диск по инструкции вверху


-------------------------------
## WSL команды

wsl --list --online               \\ список систем которые можно установить
wsl --install -d Ubuntu           \\ установить систему, например Ubuntu 
wsl --list --verbose              \\ список систем их статус и версия
wsl --unregister Ubuntu           \\ удаление Ubuntu
wsl --status                      \\ статус подсистемы
wsl.exe --update                  \\ обновить ядро
wsl.exe --set-version Ubuntu 2    \\ обновление Linux до версии 2
wsl.exe --set-default-version 2   \\ установка версии 2 по умолчанию
wsl --setdefault Ubuntu           \\ установка Ubuntu системы по умолчанию (указано звездой в wsl.exe --list --verbose)
wsl --shutdown                    \\ завершить процесс подсистемы
explorer.exe `wslpath -w "$PWD"`  \\ открыть текущую папку подсистемы в проводнике Windows
\\wsl$                            \\ в проводнике Windows ввести в адресную строку - перейти в папку подсистемы


Bat-файл для запуска WSL - создать файл с расширением .bat и прописать туда:
cmd /k wsl

Bat-файл для запуска WSL и перехода в определенную директорию (путь D:\wsl в формате Windows либо прямо из WSL /mnt/d/wsl/) 
cmd /k wsl --cd D:\wsl
(проверить правильность своего пути командой ls)

Bat-файл для запуска WSL через Windows Terminal
wt wsl --cd D:\wsl

Bat-файл для завершения работы WSL (например если заполнена ОЗУ)
wsl --shutdown

Ярлык для запуска WSL через Windows Terminal - ПКМ - создать ярлык - указать расположение
C:\Users\111\AppData\Local\Microsoft\WindowsApps\wt.exe wsl --cd /mnt/d/wsl/


-------------------------------
## Docker команды настройки

sudo service --status-all       \\ проверить статус всех служб в Linux
sudo groupadd docker            \\ создание группы чтобы потом запускать docker без sudo
sudo usermod -aG docker $USER   \\ добавить текущего пользователя в docker группу и сделать перезапуск чтобы потом запускать docker без sudo
sudo service docker start       \\ запуск docker сервиса
sudo service docker status      \\ проверка запущен ли сервис

## Docker команды
https://docs.docker.com/engine/reference/commandline/docker/ 

Образы:

docker info                         \\ info (образы лежат в /var/lib/docker/image/overlay2/imagedb/content/)
docker --version                    \\ версия (или docker version) 
docker pull busybox                 \\ загрузка образа из Docker Hub
docker images                       \\ просмотр установленных образов (или docker image ls)
docker system prune                 \\ удалить все неиспользуемые образы
docker system prune -a              \\ удалить все образы
docker rmi busybox                  \\ удалить образ
docker builder prune                \\ очистить неиспользуемый кэш сборки образов
docker builder prune -a             \\ очистить весь кэш сборки образов

Контейнеры:

docker run image-name               \\ развертывание и запуск контейнера из образа image-name
docker run --rm image-name          \\ развертывание и запуск контейнера и удаление контейнера после его остановки
docker run -d -p 8080:80 image-name \\ -d(detached) фоновый режим без консоли, -p сопоставить порт 8080 хоста с портом 80 в контейнере
docker run -it image-name           \\ запуск в интерактивном режиме с возможность использовать терминал
(-i интерактивный режим, -t подключает виртуальный терминал)

docker run image-name echo "hello"  \\запуск с параметром - команда echo внутри контейнера
docker run --rm image-name          \\ удаление контейнера после его остановки
docker run -d -P --name static-site image-name \\ --name будущее имя контейнера, -P сделает все открытые порты публичными и случайными

docker port container-id            \\ на каком порту запущен контейнер
docker ps                           с\\ писок активных контейнеров (или docker container ls)
docker ps -a						\\ список всех контейнеров
docker stop e176ec2e0cd6            \\ остановить контейнер по идентификатору из предыдущей команды
docker stop eloquent_almeida        \\ остановить контейнер по имени
docker start e176ec2e0cd6			\\ запустить контейнер по идентификатору
docker start eloquent_almeida		\\ запустить контейнер по имени
docker rm e176ec2e0cd6              у\\ далить развернутый контейнер по идентификатору можно передавать несколько id
docker rm $(docker ps -a -q -f status=exited) \\ удалить все незапущенные контейнеры
docker stop $(sudo docker ps -aq)   \\ остановить все запущенные контейнеры
docker container prune              \\ удалить все неиспользуемые контейнеры
docker attach e176ec2e0cd6          \\ подключится к запущенному контейнеру (который запущен например в фоновом режиме)
docker inspect 461d99dd749e         \\ инфо о контейнере или образе, в т.ч путь до конфигов
docker exec -it e176ec2e0cd6 bash   \\ редактировать контейнер (открытие в интерактивном режиме терминале) выйти - exit

сборка нового образа чтобы при следующих развертываниях в нем были новые установленные параметры и файлы по умолчанию
docker commit -m "Комментарий" -a "Кто изменил" container_id repository/new_image_name
docker commit -m "Add txt file ffffffff" -a "sergey" 756bb4af349e ubuntu/new_ubuntu_image
docker run -it -d -p 8080:80 ubuntu/new_ubuntu_image bash  \\ последующий запуск образа


-------------------------------
Создание Flask приложения пример в Ubuntu
https://docs.docker.com/language/python/build-images/ 

mkdir docker_project          \\ создание каталога для проекта и виртуального окружения
cd docker_project/             \\ переход в созданный каталог
sudo apt-get install python3-venv \\ установка python c поддержкой виртуальных окружений (опционально)
python3 -m venv env            \\ создание виртуального окружения (опционально)
source env/bin/activate        \\ активация виртуального окружения  (опционально)
(или env\scripts\activate в Windows)
python -m pip install flask    \\ установка Flask
pip freeze > requirements.txt  \\ копирование названий и версий библиотек python (проследить чтобы не было пакетов версии==0.0.0)
sudo nano app.py               \\ редактировать код Flask приложения в файле app.py 
python3 app.py                 \\ запуск проверка Flask приложения
sudo nano Dockerfile           \\ создание докерфайла - рецепт создания образа Docker

Пример Flask скрипта
```
from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "Hello, World!"

@app.route("/about")
def about():
    return "Инфо о сайте"
    
app.run(host='0.0.0.0')
```

Dockerfile
```
FROM python:3.8-slim-buster
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["python", "app.py"]
```

Для создания собственного контейнера, содержащего созданное Flask приложение, на основе также созданного нами
Dockerfile, находясь внутри директории docker_project необходимо выполнить команду build
В процессе выполнения команды как описано в Dockerfile будет произведена загрузка последнего образа ubuntu, внутри
него установлены все необходимые зависимости, создана директория /app , в которую будет помещено содержимое
директории flask_app/ , установлены все зависимости из файла requirements.txt , а сам образ настроен на запуск
вашего Flask приложения из директории /app , находящейся внутри образа.

sudo docker build -t flask_app:v1 .  \\ создание образа (-t присвоить образу название и версию - flask_app:v1, точка в конце - текущий путь где лежит Dockerfile)
sudo docker run -p 5000:5000 flask_app:v1     \\ обычный запуск контейнера из образа flask_app
sudo docker run -it -p 5000:5000 flask_app:v1 \\ запуск в интерактивном режиме
sudo docker run -d -p 5000:5000 flask_app:v1  \\ запуск без интерактивного режима, в фоне
после этого в браузере по адреусу http://127.0.0.1:5000/ запустится flask приложение

docker exec -it 16132485fb3e bash \\ если нужно редактировать запущенный контейнер и войти в него через bash


-------------------------------
Комментарии по переустановке Докера и Линукса в WSL:
Проверить все ли включено в компонентах винды - программы и компоненты - включение отключение компонентов - галки должны стоять на Подсистема для windows и платформа виртуальной машины

Далее переустановка линукса (вместо Ubuntu название системы, если оно отличается)
wsl --unregister Ubuntu 

Убунта удалится, нужно установить ее заново и установить докер по той же инструкции, не забыть стартануть сервис докера
sudo service docker start

Еще команда для проверки работает ли сервис - где докер должен стоять +
sudo service --status-all 

Если следующая команда сработает то докер точно работает
sudo docker run hello-world


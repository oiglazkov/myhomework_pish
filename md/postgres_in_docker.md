### Раворачивание и запуск БД PostgreSQL на удаленном сервере в Docker

1. Выполним обновление:

`sudo apt-get update`

`sudo apt-get upgrade`

2. Установим net-tools:

`sudo apt install net-tools`

3. Установим docker:

`sudo snap install docker`

4. Залогинимся в Docker:

`sudo docker login` 

далее вводим свои имя пользователя и пароль от docker-hub

5. Скачиваем PostgreSQL:

`sudo docker pull postgres`

и проверям установку:

`sudo docker images`

6. Создаем директории для контейнера и директорию для храниляща БД, на случай, если Docker перестанет работать, то все данные из БД сохранятся

`mkdir postgres`

`mkdir postgres/data`

7. Создаем файл `docker-compose.yml`, в котором пропишем все настройки.

`vi postgres/docker-compose.yml`

в файле прописываем конфигурацию:

```
version: '3.1'

services:

  db:
    image: postgres
    container_name: <your_container_name>
    restart: always
    environment:
      POSTGRES_PASSWORD: <your_password>
      POSTGRES_USER: <your_login>
      POSTGRES_DB: <your_DB_name>
      PGDATA: /home/<your_dir>/postgres/data
      POSTGRES_INITDB_ARGS: "-A md5"
    ports:
      - "5432:5432"
```

Пояснение:

`PGDATA: /home/<your_dir>/postgres/data` - путь к только что созданному хранилищу

`POSTGRES_INITDB_ARGS: "-A md5"` - указываем, чтобы доступ к БД осуществлялся по логину/паролю


сохраняем и закрываем командой

`:wq`

8. Запускаем БД

`sudo docker-compose up -d`

9. Проверяем какие порты прослушиваются:

`sudo netstat -ntpl`

![](../../Desktop/screenshort/netstat.PNG)

10. Подключаемся к своей БД из Dbeaver, где:

* host - это IP адрес вашего удаленного сервера
* port - 5432, тот, который мы прослушиваем
* login - логин, который указали в yml файле
* pass - пароль, который указали в yml файле







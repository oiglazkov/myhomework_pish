"""Example DAG demonstrating the usage of the sensor decorator."""

from __future__ import annotations

# [START tutorial]
# [START import_module]
import pendulum

from airflow.decorators import dag, task
from airflow.sensors.base import PokeReturnValue

# [END import_module]


# [START instantiate_dag]
@dag(
    schedule=None,
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
    tags=["example"],
)
def example_sensor_decorator():
    # [END instantiate_dag]

    # [START wait_function]
    # Using a sensor operator to wait for the upstream data to be ready.
    @task.sensor(poke_interval=60, timeout=3600, mode="reschedule")
    def wait_for_upstream() -> PokeReturnValue:
        return PokeReturnValue(is_done=True, xcom_value="xcom_value")

    # [END wait_function]

    # [START dummy_function]
    @task
    def dummy_operator() -> None:
        pass

    # [END dummy_function]

    # [START main_flow]
    wait_for_upstream() >> dummy_operator()
    # [END main_flow]


# [START dag_invocation]
tutorial_etl_dag = example_sensor_decorator()
# [END dag_invocation]

# [END tutorial]